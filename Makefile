run:
	echo "Iniciando banco"
	docker compose up -d base_drones_db

	sleep 3
	echo "Iniciando API"
	docker compose up api_base_drones

migration:
	docker compose exec api_base_drones poetry run alembic revision --autogenerate -m "$(message)"

migrate:
	echo "Aplicando migracoes"
	docker compose exec api_base_drones poetry run alembic upgrade head

install:
	echo "Instalando dependencias"
	docker compose run api_base_drones poetry install

add-dependency:
	docker compose exec api_base_drones poetry add $(d)
