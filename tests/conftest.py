from fastapi.testclient import TestClient

from faker import Faker

from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from sqlalchemy.orm import sessionmaker

from typing import Generator

import pytest, random

from urllib.parse import urlencode

from api_base_drones.main import app
from api_base_drones.core.database import db_url


faker: Faker = Faker()


@pytest.fixture(scope='session')
def client() -> Generator[TestClient, None, None]:
    with TestClient(app) as c:
        yield c


@pytest.fixture(scope='session')
def db() -> Generator[Session, None, None]:
    engine = create_engine(db_url)
    Session: sessionmaker = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    with Session() as sess:
        yield sess


@pytest.fixture(scope='session')
def user_random() -> dict:
    return dict(
        username=faker.user_name(),
        email=faker.email(),
        phone=str(random.randint(10000000000, 99999999999)),
        password=faker.password()
    )


def get_token(client: TestClient, conta_random: dict) -> str:
    form_data: bytes = urlencode({
        'username': conta_random['email'],
        'password': conta_random['password']
    }).encode('utf-8')
    
    response: dict[str, str] = client.post(
        '/users/login', 
        content=form_data.decode(),
        headers={'Content-Type': 'application/x-www-form-urlencoded'}
    ).json()

    return response['access_token']
