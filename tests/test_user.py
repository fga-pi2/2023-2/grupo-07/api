from faker import Faker
from fastapi.testclient import TestClient

from httpx import Response
from urllib.parse import urlencode

from .conftest import get_token

import json


faker: Faker = Faker()


def test_user_create(client: TestClient, user_random: dict) -> None:
    response: Response = client.post(
        '/users/register', content=json.dumps(user_random)
    )
    assert response.status_code == 201
    assert response.json()['username'] == user_random['username']
    assert response.json()['email'] == user_random['email']


def test_user_create_email_invalid(
    client: TestClient, user_random: dict
) -> None:
    user_random_invalid = user_random.copy()
    user_random_invalid['email'] = 'admin'

    response: Response = client.post(
        '/users/register', content=json.dumps(user_random_invalid)
    )

    assert response.status_code == 422
    assert 'The email address is not valid' in str(response.json())


def test_user_create_phone_invalid(
    client: TestClient, user_random: dict
) -> None:
    user_random_invalid = user_random.copy()
    user_random_invalid['phone'] = '61teste12'

    response: Response = client.post(
        '/users/register', content=json.dumps(user_random_invalid)
    )

    assert response.status_code == 422
    assert "String should match pattern '^[0-9]+$'" in str(response.json())


def test_user_create_name_invalid(
    client: TestClient, user_random: dict
) -> None:
    user_random_invalid = user_random.copy()
    user_random_invalid['username'] = 'a'

    response: Response = client.post(
        '/users/register', content=json.dumps(user_random_invalid)
    )

    assert response.status_code == 422
    assert 'String should have at least 3 characters' in str(response.json())


def test_login_valid(client: TestClient, user_random: dict) -> None:
    form_data: bytes = urlencode({
        'username': user_random['email'],
        'password': user_random['password']
    }).encode('utf-8')

    response: Response = client.post(
        '/users/login', 
        content=form_data.decode(),
        headers={'Content-Type': 'application/x-www-form-urlencoded'}
    )

    assert response.status_code == 200
    assert 'access_token' in response.json().keys()
    assert response.json()['token_type'] == 'bearer'


def test_login_indalid(client: TestClient, user_random: dict) -> None:
    form_data: bytes = urlencode({
        'username': user_random['email'],
        'password': faker.password()
    }).encode('utf-8')

    response: Response = client.post(
        '/users/login', 
        content=form_data.decode(),
        headers={'Content-Type': 'application/x-www-form-urlencoded'}
    )

    assert response.status_code == 401
    assert response.json()['detail'] == 'Incorrect email or password'


def test_user_me(
    client: TestClient, user_random: dict
) -> None:
    response: Response = client.get(
        '/users/me',
        headers={'Authorization': f'Bearer {get_token(client, user_random)}'}
    )

    assert response.status_code == 200
    assert response.json()['username'] == user_random['username']
    assert response.json()['email'] == user_random['email']
    assert response.json()['phone'] == user_random['phone']


def test_user_me_invalid(
    client: TestClient, user_random: dict
) -> None:
    response: Response = client.get(
        '/users/me',
        headers={'Authorization': f'Bearer testezada'}
    )

    assert response.status_code == 401
