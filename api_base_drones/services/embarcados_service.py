from fastapi import HTTPException

from sqlalchemy import select, and_, update
from sqlalchemy.orm import Session

from api_base_drones.models.drone_model import Drone
from api_base_drones.schemas.drone_schema import DroneEditSchema 


class EmbarcadosService:
    def get_rfid_drones_by_base(self, id_base: int, session: Session):
        sql_select = select(Drone.serie).where(Drone.id_base == id_base)
        result = session.execute(sql_select).scalars().all()
        return result
    
    def edit_drone(
        self,
        rfid_drone: str,
        drone_status: DroneEditSchema,
        session: Session
    ) -> Drone:
        drone = session.query(Drone).filter(Drone.serie == rfid_drone).first()

        if drone is None:
            raise HTTPException(
                status_code=404, detail='Drone não encontrado'
            )
        sql_update = update(Drone).where(Drone.serie == rfid_drone).values(
            **drone_status.model_dump(exclude_none=True)
        ).returning(Drone)
        drone = session.execute(sql_update).scalar_one_or_none()
        session.commit()

        return drone


embarcados_service = EmbarcadosService()
