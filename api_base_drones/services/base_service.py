from fastapi import HTTPException

from sqlalchemy import insert, select, update, delete, and_
from sqlalchemy.orm import Session

from api_base_drones.models.base_model import Base
from api_base_drones.schemas.base_schema import BaseEditSchema, BasePostSchema


class BaseService:
    def get_base(
        self, id_user: int, id_base: int, session: Session
    ) -> Base | None:
        sql_select = (
            select(Base)
            .where(and_(
                Base.id_user == id_user, Base.id_base == id_base
            ))
        )
        result = session.execute(sql_select).scalar_one_or_none()
        return result

    def create(self, id_user: int, new_base: BasePostSchema, session: Session):
        sql_insert = (
            insert(Base)
            .values(**new_base.model_dump(), id_user=id_user)
            .returning(Base)
        )

        result = session.execute(sql_insert)
        session.commit()
        
        return result.scalar_one()

    def edit(
        self, 
        id_user: int, 
        id_base: int, 
        base: BaseEditSchema, 
        session: Session
    ):
        if self.get_base(id_user, id_base, session) is None:
            raise HTTPException(
                status_code=404, detail='Base não encontrada'
            )

        sql_update = (
            update(Base)
                .where(Base.id_base == id_base)
                .values(**base.model_dump(exclude_none=True))
                .returning(Base)
        )
        result = session.execute(sql_update)
        session.commit()
        return result.scalar_one()

    def delete(self, id_user: int, id_base: int, session: Session):
        if self.get_base(id_user, id_base, session) is None:
            raise HTTPException(
                status_code=404, detail='Base não encontrada'
            )
        
        sql_delete = (
            delete(Base)
                .where(Base.id_base == id_base)
                .returning(Base)
        )
        session.execute(sql_delete)
        session.commit()
        return {'msg': 'Base excluida com sucesso'}
    
    def get_all(self, id_user: int, session: Session):
        sql_select = (
            select(Base).where(Base.id_user == id_user).order_by(Base.id_base)
        )
        result = session.execute(sql_select)
        return result.scalars().all()

    def get(self, id_user: int, id_base: int, session: Session) -> Base:
        base = self.get_base(id_user, id_base, session)

        if base is None:
            raise HTTPException(
                status_code=404, detail='Base não encontrada'
            )
        return base

base_service = BaseService()
