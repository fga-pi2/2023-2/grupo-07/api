from fastapi import HTTPException, status
from sqlalchemy import insert, select, update, delete
from sqlalchemy.orm import Session
from api_base_drones.core.security import get_password_hash

from api_base_drones.schemas.user_schema import UserPostSchema, UserEditSchema
from api_base_drones.models.user_model import User


class UserService:
    def create(self, new_user: UserPostSchema, session: Session):
        user = session.execute(
            select(User).where(User.email == new_user.email)
        ).first()
        
        if user:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail='Usuário já cadastrado'
            )
        new_user.password = get_password_hash(new_user.password)
        sql_insert = insert(User).values(
                    **new_user.model_dump()).returning(User)

        result = session.execute(sql_insert)
        session.commit()
        
        return result.scalar_one()

    def edit(self, id_user, user: UserEditSchema, session: Session):
        sql_update = (
            update(User)
                .where(User.id_user == id_user)
                .values(**user.model_dump(exclude_none=True))
                .returning(User)
        )
        result = session.execute(sql_update)
        session.commit()
        return result.scalar_one()

    def delete(self, id_user, session: Session):
        sql_delete = (
            delete(User)
                .where(User.id_user == id_user)
                .returning(User)
        )
        session.execute(sql_delete)
        session.commit()
        return {'msg': 'Usuário excluído com sucesso'}

user_service = UserService()
