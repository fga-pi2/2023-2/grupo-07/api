from fastapi import HTTPException

from sqlalchemy import insert, select, and_
from sqlalchemy.orm import Session

from api_base_drones.models.history_model import History 
from api_base_drones.models.drone_model import Drone 
from api_base_drones.models.base_model import Base
from api_base_drones.schemas.history_schema import (
    HistoryPostByRfidSchema,
    HistoryPostSchema
)


class HistoryService:
    def get_history(
        self, id_user: int, id_history: int, session: Session
    ) -> History | None:
        sql_select = (
            select(History)
            .where(and_(
                History.id_user == id_user,
                History.id_history == id_history
            ))
        )
        result = session.execute(sql_select).scalar_one_or_none()
        return result

    def create(
        self, new_history: HistoryPostSchema, session: Session
    ):
        # Primeiro, vamos obter a tupla correspondente a id_base na tabela Base
        sql_select_base = select(Base).where(Base.id_base == new_history.id_base)
        base = session.execute(sql_select_base).scalar_one_or_none()

        if base is None:
            raise HTTPException(
                status_code=404, detail='Base não encontrada'
            )

        id_user = base.id_user
      
        sql_insert = (
            insert(History)
            .values(
                **new_history.model_dump(), id_user=id_user)
            .returning(History)
        )

        try:
            result = session.execute(sql_insert).scalar_one()
            session.commit()
        except Exception as e:
            msg = ''
            if 'violates foreign key constraint' in str(e):
                msg = 'A base ou drone informado não existe'

            raise HTTPException(
                status_code=400, detail=f'Erro ao inserir dado no histórico. {msg}'
            )        
        return result
    
    def create_by_rfid(
        self, new_history: HistoryPostByRfidSchema, session: Session
    ):
        sql_select = select(Drone).where(Drone.serie == new_history.rfid_drone)
        drone = session.execute(sql_select).scalar_one_or_none()

        if drone is None:
            raise HTTPException(
                status_code=404, detail='Drone não encontrado'
            )
            
        sql_select_base = select(Base).where(Base.id_base == new_history.id_base)
        base = session.execute(sql_select_base).scalar_one_or_none()

        if base is None:
            raise HTTPException(
                status_code=404, detail='Base não encontrada'
            )
        
        id_user = base.id_user
        
        print(drone.id_drone)

        sql_insert = (
            insert(History)
            .values(id_drone=drone.id_drone, id_base=new_history.id_base, id_user=id_user)
        )

        try:
            session.execute(sql_insert)
            session.commit()
        except Exception as e:
            msg = ''
            if 'violates foreign key constraint' in str(e):
                msg = 'A base ou drone informado não existe'
            print(msg)
            raise HTTPException(
                status_code=400, detail=f'Erro ao inserir dado no histórico. {msg}'
            )        
        return {'message': 'Histórico criado com sucesso'}
    
    def get_all(self, id_user: int, session: Session):
        sql_select = (
            select(History)
            .where(History.id_user == id_user)
            .order_by(History.loaded_at.desc())
        )
        result = session.execute(sql_select)
        return result.scalars().all()
    
    def get_all_by_base(self, id_user: int, id_base: int, session: Session):
        sql_select = (
            select(History)
            .where(and_(
                History.id_base == id_base,
                History.id_user == id_user
            ))
            .order_by(History.loaded_at.desc())
        )
        result = session.execute(sql_select)
        return result.scalars().all()
    
    def get_all_by_drone(self, id_user: int, id_drone: int, session: Session):
        sql_select = (
            select(History)
            .where(and_(
                History.id_drone == id_drone,
                History.id_user == id_user
            ))
            .order_by(History.loaded_at.desc())
        )
        result = session.execute(sql_select)
        return result.scalars().all()

    def get(self, id_user: int, id_history: int, session: Session) -> History:
        history = self.get_history(id_user, id_history, session)

        if history is None:
            raise HTTPException(
                status_code=404, detail='Histórico não encontrado'
            )
        return history

history_service = HistoryService()
