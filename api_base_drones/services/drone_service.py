from fastapi import HTTPException

from sqlalchemy import insert, select, update, delete, and_
from sqlalchemy.orm import Session

from api_base_drones.models.drone_model import Drone 
from api_base_drones.schemas.drone_schema import (
    DronePostSchema, DroneEditSchema
)


class DroneService:
    def get_drone(
        self, id_user: int, id_drone: int, session: Session
    ) -> Drone | None:
        sql_select = (
            select(Drone)
            .where(and_(
                Drone.id_user == id_user, Drone.id_drone == id_drone
            ))
        )
        result = session.execute(sql_select).scalar_one_or_none()
        return result

    def create(
        self, id_user: int, new_drone: DronePostSchema, session: Session
    ):
        sql_insert = (
            insert(Drone)
            .values(
                **new_drone.model_dump(), id_user=id_user)
            .returning(Drone)
        )

        try:
            result = session.execute(sql_insert).scalar_one()
            session.commit()
        except Exception as e:
            msg = ''
            if 'violates foreign key constraint' in str(e):
                msg = 'A base informada não existe'

            raise HTTPException(
                status_code=400, detail=f'Erro ao inserir drone. {msg}'
            )        
        return result

    def edit(
        self, 
        id_user: int, 
        id_drone: int, 
        base: DroneEditSchema, 
        session: Session
    ):
        if self.get_drone(id_user, id_drone, session) is None:
            raise HTTPException(
                status_code=404, detail='Base não encontrada'
            )

        sql_update = (
            update(Drone)
                .where(Drone.id_drone == id_drone)
                .values(**base.model_dump(exclude_none=True))
                .returning(Drone)
        )
        result = session.execute(sql_update)
        session.commit()
        return result.scalar_one()

    def delete(self, id_user: int, id_drone: int, session: Session):
        if self.get_drone(id_user, id_drone, session) is None:
            raise HTTPException(
                status_code=404, detail='Base não encontrada'
            )
        
        sql_delete = (
            delete(Drone)
                .where(Drone.id_drone == id_drone)
                .returning(Drone)
        )
        session.execute(sql_delete)
        session.commit()
        return {'msg': 'Drone excluido com sucesso'}
    
    def get_all(self, id_user: int, session: Session):
        sql_select = (
            select(Drone)
            .where(Drone.id_user == id_user)
            .order_by(Drone.created_at.desc())
        )
        result = session.execute(sql_select)
        return result.scalars().all()
    
    def get_all_by_base(self, id_user: int, id_base: int, session: Session):
        sql_select = (
            select(Drone)
            .where(and_(
                Drone.id_user == id_user, Drone.id_base == id_base
            ))
            .order_by(Drone.created_at.desc())
        )
        result = session.execute(sql_select)
        return result.scalars().all()

    def get(self, id_user: int, id_drone: int, session: Session) -> Drone:
        base = self.get_drone(id_user, id_drone, session)

        if base is None:
            raise HTTPException(
                status_code=404, detail='Base não encontrada'
            )
        return base


drone_service = DroneService()
