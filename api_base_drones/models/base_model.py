from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, String

from datetime import datetime

from . import Base as BaseSqlalchemy


class Base(BaseSqlalchemy):
    __tablename__ = 'base'

    id_base = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(100), nullable=False)
    latitude = Column(String(50), nullable=False)
    longitude = Column(String(50), nullable=False)
    is_active = Column(Boolean, nullable=False, default=True)
    status = Column(String(20), nullable=False)
    id_user = Column(Integer, ForeignKey('user.id_user'))
    created_at = Column(DateTime, nullable=False, default=datetime.now())
