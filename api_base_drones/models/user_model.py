from sqlalchemy import Column, ForeignKey, Integer, String, DateTime

from datetime import datetime

from . import Base


class User(Base):
    __tablename__ = 'user'

    id_user = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(String(100), nullable=False)
    password = Column(String(255), nullable=False)
    email = Column(String(100), nullable=False, unique=True)
    phone = Column(String(12), nullable=False)
    created_at = Column(DateTime, nullable=False, default=datetime.now())
