from sqlalchemy import Column, ForeignKey, Integer, DateTime, func

from datetime import datetime

from . import Base


class History(Base):
    __tablename__ = 'history'

    id_history = Column(Integer, primary_key=True, autoincrement=True)
    id_drone = Column(Integer, ForeignKey('drone.id_drone'))
    id_base = Column(Integer, ForeignKey('base.id_base'))
    id_user = Column(Integer, ForeignKey('user.id_user'))
    loaded_at = Column(DateTime, nullable=False, default=func.now())
