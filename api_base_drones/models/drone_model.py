from sqlalchemy import Column, DateTime, ForeignKey, Integer, String

from datetime import datetime

from . import Base


class Drone(Base):
    __tablename__ = 'drone'

    id_drone = Column(Integer, primary_key=True, autoincrement=True)
    serie = Column(String(255), nullable=False, unique=True)
    status = Column(String(50), nullable=False)
    id_base = Column(Integer, ForeignKey('base.id_base'))
    id_user = Column(Integer, ForeignKey('user.id_user'))
    created_at = Column(DateTime, nullable=False, default=datetime.now())
    battery_percentage = Column(Integer)
