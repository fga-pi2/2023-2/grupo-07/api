from datetime import datetime
from pydantic import BaseModel


class DronePostSchema(BaseModel):
    serie: str
    status: str
    id_base: int

    class Config:
        from_attributes = True


class DroneSchema(BaseModel):
    id_drone: int
    serie: str
    status: str
    id_base: int
    id_user: int
    created_at: datetime
    battery_percentage: int | None = None

    class Config:
        from_attributes = True


class DroneEditSchema(BaseModel):
    serie: str | None = None
    status: str | None = None
    battery_percentage: int | None = None

    class Config:
        from_attributes = True
