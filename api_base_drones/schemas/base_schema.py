from datetime import datetime
from typing import Annotated
from pydantic import BaseModel, StringConstraints


class BasePostSchema(BaseModel):
    name: Annotated[str, StringConstraints(
        min_length=3,
        max_length=100,
    )]
    latitude: str
    longitude: str
    is_active: bool
    status: str

    class Config:
        from_attributes = True


class BaseEditSchema(BaseModel):
    name: Annotated[str, StringConstraints(
        min_length=3,
        max_length=100,
    )] | None = None
    latitude: str | None = None
    longitude: str | None = None
    is_active: bool | None = None
    status: str | None = None

    class Config:
        from_attributes = True


class BaseSchema(BaseModel):
    id_base: int
    name: str
    latitude: str
    longitude: str
    is_active: bool
    status: str
    id_user: int
    created_at: datetime | str

    class Config:
        orm_mode = True
        from_attributes = True
