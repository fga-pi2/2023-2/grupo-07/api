from typing import Annotated
from pydantic import BaseModel, EmailStr, StringConstraints


class UserPostSchema(BaseModel):
    username: Annotated[str, StringConstraints(
        min_length=3,
        max_length=100,
        to_lower=True,
    )]
    phone: Annotated[str, StringConstraints(
        min_length=8,
        max_length=11,
        strip_whitespace=True,
        to_lower=True,
        pattern=r'^[0-9]+$',
    )]
    password: str
    email: EmailStr

    class Config:
        from_attributes = True


class UserEditSchema(BaseModel):
    username: Annotated[str, StringConstraints(
        min_length=3,
        max_length=100,
        to_lower=True,
    )] | None = None
    phone: Annotated[str, StringConstraints(
        min_length=8,
        max_length=11,
        strip_whitespace=True,
        to_lower=True,
        pattern=r'^[0-9]+$',
    )] | None = None
    password: str | None = None

    class Config:
        from_attributes = True

class UserSchema(BaseModel):
    id_user: int
    username: str
    email: str
    phone: str | None

    class Config:
        from_attributes = True
