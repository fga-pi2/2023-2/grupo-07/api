from datetime import datetime
from pydantic import BaseModel


class HistoryPostSchema(BaseModel):
    id_base: int
    id_drone: int

    class Config:
        from_attributes = True


class HistoryPostByRfidSchema(BaseModel):
    rfid_drone: str
    id_base: int

    class Config:
        from_attributes = True


class HistorySchema(BaseModel):
    id_history: int
    id_drone: int
    id_base: int
    id_user: int | None = None
    loaded_at: datetime

    class Config:
        from_attributes = True
