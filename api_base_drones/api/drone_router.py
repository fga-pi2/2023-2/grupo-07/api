from fastapi import APIRouter, Depends, status

from sqlalchemy.orm import Session
from api_base_drones.schemas.drone_schema import (
    DronePostSchema, DroneEditSchema, DroneSchema
)

from api_base_drones.services.drone_service import drone_service 
from api_base_drones.core.database import get_session
from api_base_drones.core.security import get_current_user
from api_base_drones.models.user_model import User


router = APIRouter(prefix='/drones', tags=['Drone'])


@router.get('/{id_drone}', status_code=status.HTTP_200_OK)
def get_drone(
    id_drone: int,
    current: User = Depends(get_current_user),
    session: Session = Depends(get_session)
) -> DroneSchema:
    return drone_service.get(current.id_user, id_drone, session) # type: ignore


@router.get(
    '/', 
    status_code=status.HTTP_200_OK,
    description='Listar todos os drones vinculados a um usuario'
)
def get_all_by_user(
    id_base: int | None = None,
    current: User = Depends(get_current_user),
    session: Session = Depends(get_session)
) -> list[DroneSchema]:
    if id_base:
        return drone_service.get_all_by_base(current.id_user, id_base, session) # type: ignore
    return drone_service.get_all(current.id_user, session) # type: ignore


@router.post(
    '/', 
    status_code=status.HTTP_200_OK,
    description='Vincular um novo drone a uma base'
)
def post_drone(
    drone: DronePostSchema, 
    current: User = Depends(get_current_user),
    session: Session = Depends(get_session)
) -> DroneSchema:
    return drone_service.create(current.id_user, drone, session) # type: ignore


@router.put('/{id_drone}', status_code=status.HTTP_200_OK)
def update_drone(
    id_drone: int,
    drone: DroneEditSchema, 
    current: User = Depends(get_current_user),
    session: Session = Depends(get_session)
) -> DroneEditSchema:
    return drone_service.edit(current.id_user, id_drone, drone, session) # type: ignore


@router.delete('/{id_drone}', status_code=status.HTTP_200_OK)
def delete_drone(
    id_drone: int,
    current: User = Depends(get_current_user),
    session: Session = Depends(get_session)
):
    return drone_service.delete(current.id_user, id_drone, session) # type: ignore


