from fastapi import APIRouter

from .base_router import router as base_router
from .user_router import router as user_router
from .drone_router import router as drone_router
from .history_router import router as history_router
from .embarcados_router import router as embarcados_router


router = APIRouter()

router.include_router(user_router)
router.include_router(base_router)
router.include_router(drone_router)
router.include_router(history_router)
router.include_router(embarcados_router)