from fastapi import APIRouter, Depends, status

from sqlalchemy import select
from sqlalchemy.orm import Session
from api_base_drones.schemas.base_schema import (
    BaseSchema, BaseEditSchema, BasePostSchema
)

from api_base_drones.services.base_service import base_service 
from api_base_drones.core.database import get_session
from api_base_drones.core.security import get_current_user
from api_base_drones.models.user_model import User


router = APIRouter(prefix='/bases', tags=['Base'])


@router.post('/', status_code=status.HTTP_200_OK)
def post_base(
    user: BasePostSchema, 
    current: User = Depends(get_current_user),
    session: Session = Depends(get_session)
) -> BaseSchema:
    return base_service.create(current.id_user, user, session) # type: ignore


@router.put('/{id_base}', status_code=status.HTTP_200_OK)
def update_base(
    id_base: int,
    user: BaseEditSchema, 
    current: User = Depends(get_current_user),
    session: Session = Depends(get_session)
) -> BaseEditSchema:
    return base_service.edit(current.id_user, id_base, user, session) # type: ignore


@router.delete('/{id_base}', status_code=status.HTTP_200_OK)
def delete_base(
    id_base: int,
    current: User = Depends(get_current_user),
    session: Session = Depends(get_session)
):
    return base_service.delete(current.id_user, id_base, session) # type: ignore


@router.get('/{id_base}', status_code=status.HTTP_200_OK)
def get_base(
    id_base: int,
    current: User = Depends(get_current_user),
    session: Session = Depends(get_session)
) -> BaseSchema:
    return base_service.get(current.id_user, id_base, session) # type: ignore


@router.get('/', status_code=status.HTTP_200_OK)
def get_all_bases(
    current: User = Depends(get_current_user),
    session: Session = Depends(get_session)
) -> list[BaseSchema]:
    return base_service.get_all(current.id_user, session) # type: ignore
