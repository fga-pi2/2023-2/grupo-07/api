from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm

from sqlalchemy import select
from sqlalchemy.orm import Session

from api_base_drones.services.user_service import user_service 
from api_base_drones.core.database import get_session
from api_base_drones.core.security import (
    create_access_token, get_current_user, verify_password
)
from api_base_drones.models.user_model import User
from api_base_drones.schemas.user_schema import UserEditSchema, UserSchema
from api_base_drones.schemas.user_schema import UserPostSchema


router = APIRouter(prefix='/users', tags=['Users'])

@router.post(
    '/register', 
    status_code=status.HTTP_201_CREATED,
    description='Cadastra um novo usuário'
)
def register_user(
    new_user: UserPostSchema, session: Session = Depends(get_session)
) -> UserSchema:
    return user_service.create(new_user, session)


@router.post(
    '/login',
    status_code=status.HTTP_200_OK,
    description='Realiza o login do usuário'
)
def login_user(
    form_data: OAuth2PasswordRequestForm = Depends(),
    session: Session = Depends(get_session),
):
    user = session.scalar(select(User).where(User.email == form_data.username))
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Incorrect email or password'
        )

    if not verify_password(form_data.password, str(user.password)):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Incorrect email or password'
        )

    access_token = create_access_token(data={'sub': str(user.id_user)})
    return {'access_token': access_token, 'token_type': 'bearer'}


@router.get(
    '/me',
    status_code=status.HTTP_200_OK,
    description='Retorna os dados do usuário logado'
)
def get_user_me(
    current: User = Depends(get_current_user)
) -> UserSchema:
    return current


@router.put('/', status_code=status.HTTP_200_OK)
def update_user(
    user: UserEditSchema, 
    current: User = Depends(get_current_user),
    session: Session = Depends(get_session)
) -> UserSchema:
    return user_service.edit(current.id_user, user, session)


@router.delete('/', status_code=status.HTTP_200_OK)
def delete_user(
    current: User = Depends(get_current_user),
    session: Session = Depends(get_session)
):
    return user_service.delete(current.id_user, session)
