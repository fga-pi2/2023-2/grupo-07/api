from fastapi import APIRouter, Depends, Request, status

from sqlalchemy.orm import Session

from api_base_drones.schemas.history_schema import HistoryPostByRfidSchema
from api_base_drones.schemas.drone_schema import DroneEditSchema
from api_base_drones.services.history_service import history_service 
from api_base_drones.services.embarcados_service import embarcados_service 
from api_base_drones.services.drone_service import drone_service
from api_base_drones.core.database import get_session


router = APIRouter(prefix='/embarcados', tags=['Para embarcados'])


@router.post(
    '/history',
)
def post_history_by_rfid(
    body: HistoryPostByRfidSchema,
    session: Session = Depends(get_session),
):
    return history_service.create_by_rfid(body, session)


@router.get(
    '/rfid-drones/{id_base}',
)
def get_rfid_drones(
    id_base: int,
    session: Session = Depends(get_session),
) -> list[str]:
    return embarcados_service.get_rfid_drones_by_base(id_base, session) # type: ignore

@router.put('/drones/status/{rfid_drone}', status_code=status.HTTP_200_OK)
def update_drone_status(
    rfid_drone: str,
    drone_status: DroneEditSchema,
    session: Session = Depends(get_session)
) -> DroneEditSchema:
    return embarcados_service.edit_drone(rfid_drone, drone_status, session)
