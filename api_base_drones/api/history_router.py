from fastapi import APIRouter, Depends, Request, status

from sqlalchemy.orm import Session

from api_base_drones.schemas.history_schema import (
    HistoryPostByRfidSchema, HistoryPostSchema, HistorySchema
)
from api_base_drones.services.history_service import history_service 
from api_base_drones.core.database import get_session

from api_base_drones.models.user_model import User
from api_base_drones.core.security import get_current_user

router = APIRouter(prefix='/history', tags=['History'])


@router.get(
    '/{id_history}',
    status_code=status.HTTP_200_OK,
    description='Listar historico específico'
)
def get_history(
    id_history: int,
    session: Session = Depends(get_session),
    current: User = Depends(get_current_user),
) -> HistorySchema:
    return history_service.get(current.id_user,id_history, session) # type: ignore


@router.get(
    '/drone/{id_drone}', 
    status_code=status.HTTP_200_OK,
    description='Listar historico de um drone'
)
def get_all_by_drone(
    id_drone: int,
    session: Session = Depends(get_session),
    current: User = Depends(get_current_user),
) -> list[HistorySchema]:
    return history_service.get_all_by_drone(current.id_user,id_drone, session) # type: ignore


@router.get(
    '/base/{id_base}', 
    status_code=status.HTTP_200_OK,
    description='Listar historico de uma base'
)
def get_all_by_base(
    id_base: int,
    session: Session = Depends(get_session),
    current: User = Depends(get_current_user),
) -> list[HistorySchema]:
    return history_service.get_all_by_base(current.id_user,id_base, session) # type: ignore


@router.get(
    '/', 
    status_code=status.HTTP_200_OK,
    description='Listar historico geral'
)
def get_all(
    session: Session = Depends(get_session),
    current: User = Depends(get_current_user),
) -> list[HistorySchema]:
    return history_service.get_all(current.id_user,session) # type: ignore


@router.post(
    '/', 
    status_code=status.HTTP_200_OK,
    description='Criar um novo historico'
)
def post_history(
    history: HistoryPostSchema,
    session: Session = Depends(get_session),
) -> HistorySchema:
    return history_service.create(history, session) # type: ignore


@router.post(
    '/por-rfid',
)
def post_history_by_rfid(
    body: HistoryPostByRfidSchema,
    session: Session = Depends(get_session),
):
    return history_service.create_by_rfid(body, session)
