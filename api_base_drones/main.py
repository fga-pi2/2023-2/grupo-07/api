from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from api_base_drones.api import router as all_router

app = FastAPI()

# CORS configuration
origins = [
    "http://localhost",
    "http://localhost:3000",
    "https://dronespi2.netlify.app" # frontend host
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(all_router)
