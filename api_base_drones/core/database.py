from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import Session, sessionmaker

from typing import Iterable

from api_base_drones.core.config import settings


db_url = f'postgresql+psycopg2://{settings.USER_DB}:{settings.PASSWORD}@{settings.HOST}:{settings.DB_PORT}/{settings.DB}'
engine = create_engine(db_url)

SessionLocal = sessionmaker(
    bind=engine,
    autocommit=False,
    autoflush=False,
    expire_on_commit=False,
)

def get_session() -> Iterable[Session]:
    with SessionLocal() as session:
        yield session
