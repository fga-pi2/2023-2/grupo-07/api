from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    model_config = SettingsConfigDict(
        env_file='.env', 
        env_file_encoding='utf-8', 
        case_sensitive=True,
    )

    USER_DB: str
    PASSWORD: str
    HOST: str
    DB: str
    DB_PORT: str

    SECRET_KEY: str
    ALGORITHM: str = 'HS256'
    ACCESS_TOKEN_EXPIRE_MINUTES: float = 60*24


settings = Settings() # type: ignore
