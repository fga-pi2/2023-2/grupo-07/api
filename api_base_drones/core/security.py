from datetime import datetime, timedelta

from fastapi import HTTPException, Depends, status
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials, OAuth2PasswordBearer

from jose import JWTError, jwt

from pydantic import BaseModel

from sqlalchemy import select
from sqlalchemy.orm import Session

from passlib.context import CryptContext

from api_base_drones.core.config import settings
from api_base_drones.core.database import get_session
from api_base_drones.models.user_model import User


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="users/login")
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

class TokenData(BaseModel):
    id: int | None = None

def get_password_hash(password: str):
    return pwd_context.hash(password)


def verify_password(plain_password: str, hashed_password: str):
    return pwd_context.verify(plain_password, hashed_password)


# JWT

def create_access_token(data: dict):
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    to_encode.update({'exp': expire})
    encoded_jwt = jwt.encode(to_encode, settings.SECRET_KEY, algorithm=settings.ALGORITHM)
    return encoded_jwt

async def get_current_user(
    session: Session = Depends(get_session),
    token: str = Depends(oauth2_scheme),
):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Não foi possível validar as credenciais",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(
            token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM]
        )
        id_user = payload.get("sub")
        if id_user is None:
            raise credentials_exception
        token_data = TokenData(id=id_user)
    except JWTError as e:
        raise credentials_exception
    
    user = session.execute(
        select(User).where(User.id_user == token_data.id)
    ).scalars().first()
    
    if user is None:
        raise credentials_exception

    return user
