# API

## Execução do projeto com docker

### Passo 1:

Executar o serviço para o banco de dados e api.
```
docker compose up
```

### Passo 2:

Realizar as migrates para atualizar o banco de dados. Lembrando que esse comando deve ser executado em outro terminal, para não interromper a execução dos containeres.
```
docker compose exec api_base_drones poetry run alembic upgrade head
```

## Pré-requisitos caso não tenha Docker

Para utilizar a API IES, é necessário atender aos seguintes pré-requisitos:

- Python 3.11 (recomenda-se o uso do pyenv para facilitar a instalação).
- Poetry (gerenciador de dependências).

Obs.: [Guia de instalação dos pré-requisitos](#guia-instalacao-pre-requisitos) no fim do README.


## Como executar o projeto sem docker

### Instalação das dependências

Na primeira execução, é necessário instalar as dependências do projeto. Para isso, utilize o seguinte comando:

```sh
poetry install
```

### Ativação do ambiente virtual

Para entrar no ambiente virtual, utilize o seguinte comando:

```sh
poetry shell
```

### Execução do projeto

Após ativar o ambiente virtual, execute o seguinte comando para iniciar a API:

```sh
task start
```

Se for a primeira ver executando o projeto deve-se realizar as migrações com o comando a seguir:
```sh
alembic upgrade head
```

Caso queira rodar o projeto sem utilizar o ambiente virtual:
```sh
poetry run task start
```

<h2 id="guia-instalacao-pre-requisitos"> 
    Guia de instalação dos pré-requisitos 
</h2>

### Instalação do pyenv

Para garantir um ambiente adequado para a execução da API IES, siga os passos abaixo para instalar o pyenv e configurar a versão do Python necessária.

Siga o link a seguir para obter instruções detalhadas sobre como instalar o pyenv no seu sistema: https://www.dedicatedcore.com/blog/install-pyenv-ubuntu/

Com o arquivo .python-version na raiz do projeto, o pyenv será capaz de identificar automaticamente a versão correta do Python a ser utilizada neste repositório.

### Instalação do poetry

Baixe o instalador do Poetry usando o seguinte comando:

```sh
curl -sSL https://install.python-poetry.org | python3 -
```

Após a instalação, o Poetry irá solicitar que você adicione seu diretório binário ao seu PATH. Para fazer isso basta executar o comando a seguir:

```sh
echo 'export PATH="$PATH:$HOME/.local/bin"' >> ~/.bashrc
```

Salve o arquivo e feche-o.

Para reiniciar o shell, execute:
```sh
exec "$SHELL"
```

Para verificar se o Poetry foi instalado corretamente, execute o seguinte comando:
```sh
poetry --version
```
