FROM python:3.11.5-bullseye

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /app/

ADD . .

RUN pip install poetry
RUN poetry install

CMD ["sh", "-c", "poetry run task start"]
